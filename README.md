# minesweeper

[![Codemagic build status](https://api.codemagic.io/apps/5fceb89237b38900110ead03/5fceb89237b38900110ead02/status_badge.svg)](https://codemagic.io/apps/5fceb89237b38900110ead03/5fceb89237b38900110ead02/latest_build)

Flutter minesweeper app for coding challenge

## Getting Started

1. Clone this repository
2. Install flutter + dependencies
3. Switch to `master` channel due to Desktop & Web support
4. Run minesweeper and have fun!

## Attribution

<div>Minesweeper Icon made by <a href="https://www.flaticon.com/authors/pixel-perfect" title="Pixel perfect">Pixel perfect</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>