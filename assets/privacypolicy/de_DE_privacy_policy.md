## DigitalSweeper - App

**Einleitung**

Die Applikation "DigitalSweeper" (nachfolgend "App") wird
von Rebar Ahmad (nachfolgend "wir" oder
"uns") zur Verfügung gestellt.

Wir möchten Ihnen die größtmögliche Transparenz über die Bewegung Ihrer
Daten geben. Es ist für uns selbstverständlich, dass wir sorgfältig mit
Ihren Daten umgehen, sie sicher verwahren und nicht an Dritte
weitergeben.

Für Rückfragen zum Thema Datenschutz kontaktieren Sie bitte:

Herrn Rebar Ahmad\
[rebar.ahmad@gmail.com](mailto:rebar.ahmad@gmail.com)

Bitte Lesen Sie die folgende Datenschutzerklärung sorgfältig durch,
bevor sie die App verwenden. Beim Download, der Installation oder der
Nutzung unserer Software stimmen Sie dieser Endnutzervereinbarung zu.

Im Rahmen dieser App bieten wir Ihnen ein digitales Brettspiel an,
welches auch unter dem Namen "Minesweeper" bekannt ist.
Die App soll im Rahmen einer Coding-Challenge als Vorführmodell dienen.

Die Nutzung der App bedarf weder sensiblen personenbezogenen Daten, noch
etwaige Login-Daten. Nichtsdestotrotz: Uns ist der Schutz Ihrer 
Privatsphäre sowie die Offenlegung welche Daten erhoben werden und 
weshalb von oberster Priorität. Sie können diese Datenschutzerklärung 
jederzeit unter dem Menüpunkt "Datenschutzerklärung" innerhalb der App einsehen.

**§ A -- Allgemeines**

1.  Nur wenn Sie uns per Post, E-Mail oder auf anderem Wege
personenbezogene Daten übermitteln, werden zusätzliche Angaben
zwecks Bearbeitung der Anfrage und für den Fall von
Anschlussfragen bei uns gespeichert. Diese Daten geben wir nicht
ohne Ihre Einwilligung weiter.

2.  Sie können die Einwilligung der Verarbeitung Ihrer personenbezogenen Daten jederzeit widerrufen.

3.  Sie haben jederzeit das Recht, Beschwerde bei der entsprechenden Aufsichtsbehörde einzureichen.

4.  Sie haben jederzeit das Recht von uns auf Antrag eine Auskunft über die von uns verarbeiteten,
Sie betreffenden personenbezogenen Daten nach Art. 15 DS-GVO zu erhalten.

5.  Sie haben jederzeit das Recht, die unverzügliche Berichtigung der Sie betreffenden Daten zu verlangen, sofern diese unrichtig sein sollten.

6.  Bestimmte Informationen werden bereits automatisch verarbeitet, sobald Sie die App verwenden. Welche personenbezogenen Daten genau verarbeitet werden, haben wir im Folgenden für Sie aufgeführt:

a.  **Informationen beim Download**

Beim Herunterladen der App werden bestimmte erforderliche
Informationen an den von Ihnen ausgewählten App Store übermittelt, 
insbesondere können dabei Nutzername, E-Mail-Adresse, Kundennummer 
Ihres Store Accounts, Zeitpunkt des Downloads sowie die individuelle 
Gerätekennziffer verarbeitet werden. Die Verarbeitung dieser Daten erfolgt
ausschließlich durch den jeweiligen App-Store und liegt außerhalb
unseres Einflussbereiches.

b.  **Informationen, die automatisch erhoben werden**

Im Rahmen der Nutzung der App erheben wir bestimmte Daten, die
notwendig sind zur Nutzung der App. Hierzu gehören:

-   IP-Adresse

-   Datum und Uhrzeit der Anfrage

-   Inhalt der Anforderung

-   Jeweils übertragene Datenmenge

-   Betriebssystem

-   http-Statuscode

-   Inhalt von der die Anforderung kommt

-   Oberfläche des Betriebssystems

-   Version der App

Diese Daten werden automatisch übermittelt und dreißig Tage
gespeichert, um den Dienst und die damit verbundenen Funktionen
anzubieten; die Funktionen der App zu erfüllen sowie Missbrauch sowie
Fehlfunktionen zu antizipieren und präventiv zu wirken. Diese
Datenvereinbarung ist notwendiger Bestandteil für die Erfüllung des
Vertrags zwischen Ihnen als Nutzer\in und uns gemäß Art. 6 Abs. 1 lit
b) DS-GVO und somit gerechtfertigt.

c.  **Anmeldung in der App und Erstellung eines Nutzerkontos**

Zur Nutzung dieser App ist die Erstellung eines Nutzerkontos keine
Voraussetzung.

d.  **Zugriffe und Berechtigungen**

-   Vibrationsalarm:
    Diese Berechtigung wird für die Wiedergabe von haptischem Feedback benötigt, 
    sobald der Nutzer beispielsweise Felder aufdeckt, oder auf eine Mine gekommen ist.

Diese Datenvereinbarung ist notwendiger Bestandteil der die Erfüllung
des Vertrags zwischen Ihnen als Nutzer\in und uns gemäß Art. 6 Abs. 1
lit b) DS-GVO und somit gerechtfertigt.

**B -- Weitergabe und Übertragung von Daten**

Wenn es zur Aufklärung einer rechtswidrigen oder missbräuchlichen
Nutzung der App oder für die Rechtsverfolgung erforderlich ist, werden
personenbezogene Daten an die Strafverfolgungsbehörden oder andere
Behörden weitergeleitet. Eine Weitergabe kann auch dann stattfinden,
wenn dies der Durchsetzung von Nutzungsbedingungen oder anderen
Rechtsansprüchen dient.\
Eine solche Weitergabe der personenbezogenen Daten ist dadurch
gerechtfertigt, dass die Verarbeitung zur Erfüllung einer rechtlichen
Verpflichtung erforderlich ist, der wir gemäß Art 6. Abs. 1 DS-GVO
unterliegen.

**C -- Datenübermittlung in Drittländer**

Sämtliche Daten werden nicht außerhalb Deutschlands verwahrt und
verarbeitet. Ein Drittstaatentransfer erfolgt nicht. 

**D -- Zeitraum der Datenspeicherung**

Da keine personenbezogenen Daten gespeichert werden,
fällt auch der Zweckbindungsgrundsatz aus.

**E -- Beschwerderecht**

Sie haben ferner das Recht, sich bei Beschwerden an die zuständige
Aufsichtsbehörde zu wenden. Diese ist: Der Hamburgische Beauftragte
für Datenschutz und Informationsfreiheit - Prof. Dr. Johannes Caspar

**F -- Änderungen der Datenschutzerklärung**

Um sich der Dynamik und ständig wechselnden Angeboten anzupassen
behalten wir uns vor diese Datenschutzerklärung fortlaufen anzupassen.
Die aktuelle und gültige Fassung ist stets die in der App abrufbare.

**Stand: 09.12.2020**
