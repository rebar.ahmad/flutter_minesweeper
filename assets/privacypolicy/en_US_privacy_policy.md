## DigitalSweeper - App

**Introduction**

The "DigitalSweeper" application (hereinafter "App") is
by Rebar Ahmad (hereinafter "we" or
"us").

We would like to offer you the greatest possible transparency regarding the movement of your
Give data. It is a matter of course for us that we carefully
handle your data, keep it safe and do not pass it on to third
pass on.

Please contact us if you have any questions about data protection:

Mr Rebar Ahmad\
[rebar.ahmad@gmail.com](mailto:rebar.ahmad@gmail.com)

Please read the following privacy policy carefully,
before you use the app. When downloading, installing or
Use of our software you agree to this end user agreement.

As part of this app we offer you a digital board game,
which is also known as "Minesweeper".
The app is to be used as a demonstration model within the framework of a coding challenge.

The use of the App requires neither sensitive personal data nor
any login data. Nevertheless: We are committed to the protection of your 
privacy and the disclosure of which data is collected and 
which is why it is a top priority. You can download this privacy policy 
at any time under the menu item "Privacy Policy" within the app.

**§ A -- General**

1. only if you send us a request by post, e-mail or other means
personal data, additional information will be provided to the
for the purpose of processing the request and in the event
connection questions are stored with us. We do not give this data
without your consent.

2. you can revoke your consent to the processing of your personal data at any time.

3. you have the right to lodge a complaint with the relevant supervisory authority at any time.

4. you have the right at any time to request information from us about the data processed by us,
to receive personal data concerning you in accordance with Art. 15 DS-GVO

5. you have the right at all times to demand that the data concerning you be corrected immediately if it is incorrect.

6) Certain information is already processed automatically as soon as you use the App. We have listed below which personal data are processed exactly:

a.  **Information when downloading**

When downloading the app, certain required
information to the App Store you selected, 
in particular, user name, e-mail address, customer number 
of your store account, time of download and the individual 
device code number can be processed. The processing of this data is
exclusively through the respective App-Store and is located outside
of our sphere of influence.

b.  **Information that is collected automatically**

In the course of using the app, we collect certain data that
are necessary to use the App. These include:

- IP address

- Date and time of the request

- Content of the request

- Amount of data transmitted in each case

- Operating system

- http status code

- Content from which the request comes

- User interface of the operating system

- Version of the app

These data are transmitted automatically and thirty days
to save the service and related functions
the functions of the App and to prevent abuse and misuse
to anticipate malfunctions and take preventive action. This
Data agreement is a necessary component for the fulfilment of the
Agreement between you as user\in and us pursuant to Art. 6 para. 1 lit.
(b) DS-BER and therefore justified.

c.  **Registration in the app and creation of a user account**

To use this app, the creation of a user account is not
prerequisite.

d.  **Accesses and authorisations**

- Vibrating alarm:
    This authorisation is required for the reproduction of haptic feedback, 
    as soon as the user uncovers fields, for example, or has come across a mine.

This data agreement is a necessary part of the fulfilment
of the agreement between you as user\in and us pursuant to Art. 6 paragraph 1
lit b) DS-GVO and therefore justified.

**B -- Transmission and transfer of data**

If it is necessary to investigate an illegal or abusive
use of the app or is required for legal prosecution, will
personal data to law enforcement authorities or other
authorities. A transfer can also take place then,
if this is necessary to enforce the terms of use or other
legal claims serves.\
Such a passing on of personal data is thereby
justifies that the processing is necessary for compliance with a legal
obligation, which we are obliged to fulfil in accordance with Art. 6 Para. 1 DS-GVO
are subject to.

**C -- Transfer of data to third countries**

All data will not be stored outside Germany and
processed. There is no transfer to third countries. 

**D -- Data storage period**

As no personal data is stored,
the principle of purpose limitation is also absent.

**E -- Right of appeal**

They also have the right to address complaints to the competent
supervisory authority. This is: The Hamburg Commissioner
for data protection and freedom of information - Prof. Dr. Johannes Caspar

**F -- Changes to the privacy policy**

To adapt to the dynamics and constantly changing offers
we reserve the right to adapt this data protection declaration.
The current and valid version is always the version available in the app.

**Status: 09.12.2020**