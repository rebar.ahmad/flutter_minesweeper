import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';
import 'package:minesweeper/GameScreen/dialogs/GameStateDialogs.dart';
import 'package:minesweeper/GameScreen/model/CellController.dart';
import 'package:minesweeper/GameScreen/model/CellCoordinates.dart';
import 'package:minesweeper/GameScreen/service.dart';
import 'package:minesweeper/GameScreen/tools/HapticFeedback.dart';
import 'package:minesweeper/GameScreen/widgets/Cell.dart';
import 'package:minesweeper/GameScreen/widgets/StatusDisplay.dart';
import 'package:minesweeper/app/constants/MediaHelper.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';
import 'package:provider/provider.dart';

class GameScreen extends StatefulWidget {
  const GameScreen({
    Key key,
  }) : super(key: key);

  @override
  _GameScreenState createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> {
  GameNotifier gameService;
  int mines;
  List<List<Cell>> board = <List<Cell>>[];
  Widget gameBoard;
  Timer runningGame;
  int runningCounter = 0;
  int flagCounter = 0;
  int cellsLeft = 0;

  @override
  void initState() {
    super.initState();
    gameService = Provider.of<GameNotifier>(context, listen: false);
    initGameBoard();
    initGameChangedListener();
  }

  @override
  void dispose() {
    gameService.removeListener(() {});
    super.dispose();
  }

  void initGameChangedListener() {
    gameService.addListener(() {
      if (mounted) initGameBoard();
    });
  }

  void startTimer() {
    runningGame ??= Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        runningCounter++;
      });
    });
  }

  void initGameBoard() {
    runningGame?.cancel();
    runningGame = null;
    runningCounter = 0;

    if (mounted) restart();
  }

  void restart() {
    cellsLeft = gameService.numberOfFields * gameService.numberOfFields;

    board = List<List<Cell>>.generate(
      gameService.numberOfFields,
      (i) => List<Cell>.generate(
        gameService.numberOfFields,
        (j) => Cell(
          key: ValueKey('cell_${i}_$j'),
          onTap: onCellTap,
          onLongPress: onCellLongPress,
          controller: CellController(
            coordinates: CellCoordinates(0, 0),
            backgroundColor: Colors.grey,
            numberOfAdjacentMines: 0,
            isMine: false,
            revealed: false,
            flagged: false,
          ),
        ),
      ),
    );

    initCells();

    setState(() {});
  }

  void initCells() {
    List<Cell> gridWidgets = <Cell>[];
    List<Row> rows = <Row>[];

    /// rows
    for (num i = 0; i < gameService.numberOfFields; i++) {
      List<Cell> cellWidgets = <Cell>[];
      /// columns
      for (num j = 0; j < gameService.numberOfFields; j++) {
        Cell nthCell = board[i][j];
        nthCell.controller.coordinates = CellCoordinates(i, j);
        cellWidgets.add(nthCell);
        gridWidgets.add(nthCell);
      }
      rows.add(
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: cellWidgets,
        ),
      );
    }

    generateMines();

    calculateAdjacentMines();

    if (gameService.numberOfFields > 10) {
      gameBoard = Center(
        key: const ValueKey('gameBoard'),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            padding: const EdgeInsets.all(10),
            child: Container(
              alignment: Alignment.topCenter,
              child: Column(
                children: rows,
              ),
            ),
          ),
        ),
      );
    } else {
      gameBoard = Center(
        key: const ValueKey('gameBoard'),
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(10),
            alignment: Alignment.center,
            width: kPhabletBreakpoint,
            height: kPhabletBreakpoint,
            child: GridView.builder(
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: gameService.numberOfFields,
              ),
              itemBuilder: (_, i) => gridWidgets[i],
              itemCount: gridWidgets.length,
            ),
          ),
        ),
      );
    }

    setState(() {});
  }

  void generateMines() {
    /// ---Erklärung---
    /// Man nehme z.B.: (10 * 10) * 0.1 = 10 Minen für ein Feld von 100 Zellen (10% Minen pro Feldgröße meint Schwierigkeitsgrad: Normal).
    /// Schwierigkeitsgrad könnte als weitere Option in den Spiele-Einstellungen gesetzt werden, um die Anzahl der Minen zu erhöhen.
    mines = ((gameService.numberOfFields * gameService.numberOfFields) * 0.1)
        .toInt();
    flagCounter = mines;

    for (var n = 0; n < mines; n++) {
      int randomX = Random().nextInt(gameService.numberOfFields);
      int randomY = Random().nextInt(gameService.numberOfFields);
      while (board[randomX][randomY].controller.isMine) {
        randomX = Random().nextInt(gameService.numberOfFields);
        randomY = Random().nextInt(gameService.numberOfFields);
      }
      Cell bombCell = board[randomX][randomY];
      bombCell.controller.isMine = true;
      board[randomX][randomY] = bombCell;
    }
  }

  void calculateAdjacentMines() {
    for (int i = 0; i < gameService.numberOfFields; i++) {
      for (int j = 0; j < gameService.numberOfFields; j++) {
        if (i > 0 && j > 0) {
          if (board[i - 1][j - 1].controller.isMine) {
            board[i][j].controller.numberOfAdjacentMines++;
          }
        }

        if (i > 0) {
          if (board[i - 1][j].controller.isMine) {
            board[i][j].controller.numberOfAdjacentMines++;
          }
        }

        if (i > 0 && j < gameService.numberOfFields - 1) {
          if (board[i - 1][j + 1].controller.isMine) {
            board[i][j].controller.numberOfAdjacentMines++;
          }
        }

        if (j > 0) {
          if (board[i][j - 1].controller.isMine) {
            board[i][j].controller.numberOfAdjacentMines++;
          }
        }

        if (j < gameService.numberOfFields - 1) {
          if (board[i][j + 1].controller.isMine) {
            board[i][j].controller.numberOfAdjacentMines++;
          }
        }

        if (i < gameService.numberOfFields - 1 && j > 0) {
          if (board[i + 1][j - 1].controller.isMine) {
            board[i][j].controller.numberOfAdjacentMines++;
          }
        }

        if (i < gameService.numberOfFields - 1) {
          if (board[i + 1][j].controller.isMine) {
            board[i][j].controller.numberOfAdjacentMines++;
          }
        }

        if (i < gameService.numberOfFields - 1 &&
            j < gameService.numberOfFields - 1) {
          if (board[i + 1][j + 1].controller.isMine) {
            board[i][j].controller.numberOfAdjacentMines++;
          }
        }
      }
    }
  }

  void onCellTap(int x, int y) {
    startTimer();

    Cell cell = board[x][y];
    cell.controller.revealed = true;

    vibrate(type: FeedbackType.selection);

    cellsLeft--;

    if (cell.controller.isMine) {
      Future.delayed(const Duration(milliseconds: 300), () async {
        revealAllMines(cell);
        await vibrate(type: FeedbackType.error);
        await GameStateDialogs.showGameOverDialog(context);
        initGameBoard();
      });
      return;
    } else {
      checkAdjacentCells(x, y);
    }

    if (cellsLeft <=
        ((gameService.numberOfFields * gameService.numberOfFields) * 0.1)
            .toInt()) {
      Future.delayed(const Duration(milliseconds: 300), () async {
        revealAllMines(null);
        await vibrate(type: FeedbackType.success);
        await GameStateDialogs.showWinDialog(context);
        initGameBoard();
      });
    }

    setState(() {});
  }

  void onCellLongPress(int x, int y) {
    Cell cell = board[x][y];

    cell.controller.flagged = !cell.controller.flagged;

    vibrate(type: FeedbackType.medium);

    if (!cell.controller.flagged) {
      setState(() {
        flagCounter++;
      });
    } else {
      setState(() {
        flagCounter--;
      });
    }

    checkFlaggedMines();

    startTimer();
  }

  void checkAdjacentCells(x, y) {
    Cell cell = board[x][y];
    if (!cell.controller.revealed && !cell.controller.flagged) {
      cell.controller.revealed = true;
      cellsLeft--;
    }

    setState(() {});

    if (x > 0) {
      final CellCoordinates coord = CellCoordinates(x - 1, y);

      if (!board[x - 1][y].controller.isMine &&
          board[coord.x][coord.y].controller.revealed != true) {
        if (cell.controller.numberOfAdjacentMines == 0) {
          checkAdjacentCells(x - 1, y);
        }
      }
    }

    if (y > 0) {
      final CellCoordinates coord = CellCoordinates(x, y - 1);
      if (!board[x][y - 1].controller.isMine &&
          board[coord.x][coord.y].controller.revealed != true) {
        if (cell.controller.numberOfAdjacentMines == 0) {
          checkAdjacentCells(x, y - 1);
        }
      }
    }

    if (y < gameService.numberOfFields - 1) {
      final CellCoordinates coord = CellCoordinates(x, y + 1);
      if (!board[x][y + 1].controller.isMine &&
          board[coord.x][coord.y].controller.revealed != true) {
        if (cell.controller.numberOfAdjacentMines == 0) {
          checkAdjacentCells(x, y + 1);
        }
      }
    }

    if (x < gameService.numberOfFields - 1) {
      final CellCoordinates coord = CellCoordinates(x + 1, y);
      if (!board[x + 1][y].controller.isMine &&
          board[coord.x][coord.y].controller.revealed != true) {
        if (cell.controller.numberOfAdjacentMines == 0) {
          checkAdjacentCells(x + 1, y);
        }
      }
    }
  }

  void revealAllMines(Cell steppedMine) {
    if (steppedMine != null)
      steppedMine.controller.backgroundColor = Colors.red.shade800;

    for (var i = 0; i < gameService.numberOfFields; i++) {
      for (var j = 0; j < gameService.numberOfFields; j++) {
        Cell cell = board[i][j];
        if (cell.controller.isMine) {
          cell.controller.revealed = true;
        }
      }
    }
  }

  void checkFlaggedMines() {
    int flaggedMines = 0;

    for (var i = 0; i < gameService.numberOfFields; i++) {
      for (var j = 0; j < gameService.numberOfFields; j++) {
        Cell cell = board[i][j];
        if (cell.controller.isMine && cell.controller.flagged) {
          flaggedMines++;
        }
      }
    }

    int mines = ((gameService.numberOfFields * gameService.numberOfFields) * 0.1)
        .toInt();
    if (flaggedMines == mines && flagCounter == 0) {
      Future.delayed(const Duration(milliseconds: 300), () async {
        revealAllMines(null);
        await vibrate(type: FeedbackType.success);
        await GameStateDialogs.showWinDialog(context);
        initGameBoard();
      });
    }
  }

  Future<void> vibrate({FeedbackType type = FeedbackType.light}) async {
    if (Theme.of(context).platform == TargetPlatform.android ||
        Theme.of(context).platform == TargetPlatform.iOS ||
        Theme.of(context).platform == TargetPlatform.fuchsia) {
      await HapticFeedback.vibrate(type: type);
    }
   }

  @override
  Widget build(BuildContext context) {
    return Consumer<GameNotifier>(
      builder: (_, notifier, widget) => Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: StatusDisplay(
            key: const ValueKey('statusDisplay'),
            numberOfMines: flagCounter,
            onReset: initGameBoard,
            count: runningCounter,
          ),
        ),
        body: AnimatedSwitcher(
          duration: const Duration(milliseconds: 250),
          switchInCurve: Curves.easeInCubic,
          switchOutCurve: Curves.easeOutCubic,
          child: gameBoard != null
              ? gameBoard
              : Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const CircularProgressIndicator.adaptive(),
                      Container(
                        padding:
                            const EdgeInsets.only(top: 15, left: 16, right: 16),
                        child: Text(
                          GothaerLocalizations.of(context)
                              .translate('LOADING_LABEL'),
                        ),
                      ),
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
