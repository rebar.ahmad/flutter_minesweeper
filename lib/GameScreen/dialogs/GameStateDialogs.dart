import 'package:flutter/material.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';

class GameStateDialogs {
  /// Sets the standard Dialog shape (here rounded dialogs)
  static ShapeBorder standardShape =
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(15));

  /// Default constraints
  static BoxConstraints constraints = const BoxConstraints(
    maxWidth: 400,
    maxHeight: 400,
  );

  static Future<void> showWinDialog(context) async {
    await showDialog(
      context: context,
      barrierDismissible: true,
      builder: (_) => GestureDetector(
        onTap: () => Navigator.of(context).pop(),
        child: AlertDialog(
          key: const ValueKey('winDialog'),
          contentPadding: EdgeInsets.zero,
          shape: standardShape,
          content: ConstrainedBox(
            constraints: constraints,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: Stack(
                children: [
                  Image.asset(
                    'assets/images/won.gif',
                    height: 300,
                    fit: BoxFit.cover,
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          height: 60,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Colors.black.withOpacity(0),
                                Colors.black.withOpacity(.75),
                                Colors.black.withOpacity(0),
                              ],
                              stops: [
                                0.0,
                                0.5,
                                1.0,
                              ],
                            ),
                          ),
                        ),
                        Text(
                          GothaerLocalizations.of(context)
                                  .translate('WIN_LABEL')
                                  .toUpperCase() +
                              '!',
                          textAlign: TextAlign.center,
                          textScaleFactor: 3.0,
                          style:
                              const TextStyle().copyWith(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  static Future<void> showGameOverDialog(context) async {
    await showDialog(
      context: context,
      barrierDismissible: true,
      builder: (_) => GestureDetector(
        onTap: () => Navigator.pop(context),
        child: AlertDialog(
          key: const ValueKey('lostDialog'),
          contentPadding: EdgeInsets.zero,
          shape: standardShape,
          content: ConstrainedBox(
            constraints: constraints,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: Stack(
                children: [
                  Image.asset(
                    'assets/images/gameover.gif',
                    height: 300,
                    fit: BoxFit.cover,
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          height: 60,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Colors.black.withOpacity(0),
                                Colors.black.withOpacity(.75),
                                Colors.black.withOpacity(0),
                              ],
                              stops: [
                                0.0,
                                0.5,
                                1.0,
                              ],
                            ),
                          ),
                        ),
                        Text(
                          GothaerLocalizations.of(context)
                              .translate('GAME_OVER_LABEL')
                              .toUpperCase(),
                          textAlign: TextAlign.center,
                          textScaleFactor: 3.0,
                          style:
                              const TextStyle().copyWith(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
