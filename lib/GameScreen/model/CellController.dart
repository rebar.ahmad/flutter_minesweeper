import 'package:flutter/material.dart';
import 'package:minesweeper/GameScreen/model/CellCoordinates.dart';

class CellController extends ChangeNotifier {
  bool _revealed;
  bool _flagged;
  CellCoordinates _coordinates;
  bool _isMine;
  num _numberOfAdjacentMines;
  Color _backgroundColor;

  bool get revealed => _revealed;

  set revealed(bool value) {
    if (value == null || value == _revealed) {
      return;
    }

    _revealed = value;
    notifyListeners();
  }

  bool get flagged => _flagged;

  set flagged(bool value) {
    if (value == null || value == _flagged) {
      return;
    }

    _flagged = value;
    notifyListeners();
  }

  CellCoordinates get coordinates => _coordinates;

  set coordinates(CellCoordinates value) {
    if (value == null || value == _coordinates) {
      return;
    }

    _coordinates = value;
    notifyListeners();
  }

  bool get isMine => _isMine;

  set isMine(bool value) {
    if (value == null || value == _isMine) {
      return;
    }

    _isMine = value;
    notifyListeners();
  }

  num get numberOfAdjacentMines => _numberOfAdjacentMines;

  set numberOfAdjacentMines(num value) {
    if (value == null || value == _numberOfAdjacentMines) {
      return;
    }

    _numberOfAdjacentMines = value;
    notifyListeners();
  }

  Color get backgroundColor => _backgroundColor;

  set backgroundColor(Color value) {
    if (value == null || value == _backgroundColor) {
      return;
    }

    _backgroundColor = value;
    notifyListeners();
  }

  CellController({
    @required revealed,
    @required flagged,
    @required coordinates,
    @required isMine,
    @required numberOfAdjacentMines,
    backgroundColor,
  })  : _revealed = revealed,
        _flagged = flagged,
        _coordinates = coordinates,
        _isMine = isMine,
        _numberOfAdjacentMines = numberOfAdjacentMines,
        _backgroundColor = backgroundColor ?? Colors.grey;

  @override
  String toString() {
    return 'CellController{_revealed: $_revealed, _flagged: $_flagged, _coordinates: ${_coordinates.toString()}, _isMine: $_isMine, _numberOfAdjacentMines: $_numberOfAdjacentMines}';
  }
}
