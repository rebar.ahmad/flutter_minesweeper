class CellCoordinates {
  int x;
  int y;

  CellCoordinates(this.x, this.y);

  @override
  String toString() {
    return 'CellCoordinates{x: $x, y: $y}';
  }
}