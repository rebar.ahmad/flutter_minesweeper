import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

class GameNotifier extends ChangeNotifier {
  static Box box;

  num _numberOfFields;

  bool _startedGame;

  num get numberOfFields => _numberOfFields;

  set numberOfFields(num value) {
    if (_numberOfFields == value) {
      return;
    }
    _numberOfFields = value;
    box.put('number.fields', value);
    notifyListeners();
  }

  bool get startedGame => _startedGame;

  set startedGame(bool value) {
    if (_startedGame == value) {
      return;
    }
    _startedGame = value;
    notifyListeners();
  }

  static Future<GameNotifier> init() async {
    if (!kIsWeb) Hive.init((await getApplicationDocumentsDirectory()).path);
    if (!Hive.isBoxOpen('DSBox')) {
      box = await Hive.openBox('DSBox');
    } else {
      box = Hive.box('DSBox');
    }

    final numOfFields = (await box.get('number.fields')) ?? 10;
    final startedGame = false;

    return GameNotifier._(numOfFields, startedGame);
  }

  GameNotifier._(
    num numOfFields,
    bool startedGame,
  )   : _numberOfFields = numOfFields,
        _startedGame = startedGame;
}
