import 'package:flutter_vibrate/flutter_vibrate.dart';

class HapticFeedback {
  static Future<void> vibrate({
    FeedbackType type,
  }) async {
    bool canVibrate = await Vibrate.canVibrate;

    if (canVibrate) {
      Vibrate.feedback(type);
    }
  }
}
