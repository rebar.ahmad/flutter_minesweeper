import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:minesweeper/GameScreen/model/CellController.dart';
import 'package:minesweeper/GameScreen/widgets/NeumorphicContainer.dart';
import 'package:tinycolor/tinycolor.dart';

typedef CellDetails = void Function(int x, int y);

class Cell extends StatefulWidget {
  const Cell({
    Key key,
    @required this.onTap,
    @required this.onLongPress,
    this.controller,
  }) : super(key: key);

  final CellController controller;
  final CellDetails onTap;
  final CellDetails onLongPress;

  @override
  _CellState createState() => _CellState();
}

class _CellState extends State<Cell> {
  Widget getRevealedChild() {
    if (widget.controller.numberOfAdjacentMines > 0) {
      final textColor = widget.controller.numberOfAdjacentMines >= 3
          ? Colors.red.shade700
          : widget.controller.numberOfAdjacentMines == 2
              ? Colors.green.shade900
              : Colors.black54;

      return Container(
        key: ValueKey(
            'cellWithAdjacentMines_${widget.controller.coordinates.x}_${widget.controller.coordinates.y}_${widget.controller.numberOfAdjacentMines}'),
        decoration: BoxDecoration(
          color:
              TinyColor(widget.controller.backgroundColor).brighten(15).color,
          borderRadius: BorderRadius.circular(2.5),
        ),
        alignment: Alignment.center,
        child: FittedBox(
          fit: BoxFit.fill,
          child: AutoSizeText(
            '${widget.controller.numberOfAdjacentMines}',
            maxLines: 1,
            textAlign: TextAlign.center,
            style: const TextStyle().copyWith(
              color: textColor,
              fontSize: 100,
            ),
          ),
        ),
      );
    } else {
      return Container(
        key: ValueKey(
            'cellRevealed_${widget.controller.coordinates.x}_${widget.controller.coordinates.y}_${widget.controller.revealed}'),
        decoration: BoxDecoration(
          color:
              TinyColor(widget.controller.backgroundColor).brighten(15).color,
          borderRadius: BorderRadius.circular(2.5),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final backgroundColor =
        TinyColor(widget.controller.backgroundColor).darken().color;

    return AnimatedBuilder(
      animation: widget.controller,
      builder: (_, animation) => GestureDetector(
        key: ValueKey(
            'cellRevealed_${widget.controller.coordinates.x}_${widget.controller.coordinates.y}_${widget.controller.revealed}'),
        onTap: () {
          if (!widget.controller.flagged && !widget.controller.revealed) {
            widget.onTap(widget.controller.coordinates.x,
                widget.controller.coordinates.y);
            setState(() {});
          }
        },
        onLongPress: () {
          if (!widget.controller.revealed) {
            widget.onLongPress(widget.controller.coordinates.x,
                widget.controller.coordinates.y);
            setState(() {});
          }
        },
        child: Container(
          key: ValueKey(
              'cellIsMine_${widget.controller.coordinates.x}_${widget.controller.coordinates.y}_${widget.controller.isMine}'),
          padding: const EdgeInsets.all(2),
          height: 30,
          width: 30,
          child: widget.controller.revealed
              ? widget.controller.isMine
                  ? Container(
                      decoration: BoxDecoration(
                        color: TinyColor(widget.controller.backgroundColor)
                            .brighten(15)
                            .color,
                        borderRadius: BorderRadius.circular(2.5),
                      ),
                      padding: const EdgeInsets.all(2),
                      alignment: Alignment.center,
                      child: Image.asset(
                        'assets/icons/bomb.png',
                        fit: BoxFit.contain,
                      ),
                    )
                  : getRevealedChild()
              : widget.controller.flagged
                  ? NeumorphicContainer(
                      key: ValueKey(
                          'cellFlagged_${widget.controller.coordinates.x}_${widget.controller.coordinates.y}_${widget.controller.flagged}'),
                      bevel: 0.25,
                      color: backgroundColor,
                      child: FittedBox(
                        fit: BoxFit.fill,
                        child: Icon(
                          CupertinoIcons.flag,
                          color: TinyColor(Theme.of(context).buttonColor)
                              .darken(20)
                              .color,
                        ),
                      ),
                    )
                  : NeumorphicContainer(
                      bevel: 0.35,
                      color: backgroundColor,
                    ),
        ),
      ),
    );
  }
}
