import 'package:flutter/material.dart';
import 'package:minesweeper/app/constants/MediaHelper.dart';

class StatusDisplay extends StatelessWidget {
  const StatusDisplay({
    Key key,
    this.numberOfMines,
    this.onReset,
    this.count,
  }) : super(key: key);

  final int numberOfMines;
  final VoidCallback onReset;
  final num count;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      height: 50,
      constraints: MediaHelper.phabletConstraints,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.black.withOpacity(0),
            Colors.black.withOpacity(.75),
            Colors.black.withOpacity(0),
          ],
          stops: [
            0.0,
            0.5,
            1.0,
          ],
        ),
      ),
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Positioned(
            left: 0,
            bottom: 0,
            top: 0,
            child: Text(
              '$numberOfMines',
              key: const ValueKey('minesCounter'),
              textScaleFactor: 2,
              style: const TextStyle().copyWith(
                shadows: <Shadow>[
                  const Shadow(
                    color: Colors.black,
                    blurRadius: 2.5,
                  ),
                ]
              ),
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: Center(
              child: IconButton(
                key: const ValueKey('resetGameButton'),
                onPressed: onReset,
                color: Colors.white,
                icon: const Icon(Icons.emoji_emotions_outlined),
              ),
            ),
          ),
          Positioned(
            right: 0,
            bottom: 0,
            top: 0,
            child: Text(
              '$count',
              key: const ValueKey('timeCounter'),
              textScaleFactor: 2,
              style: const TextStyle().copyWith(
                  shadows: <Shadow>[
                    const Shadow(
                      color: Colors.black,
                      blurRadius: 2.5,
                    ),
                  ]
              ),
            ),
          ),
        ],
      ),
    );
  }
}
