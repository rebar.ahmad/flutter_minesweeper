import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';
import 'package:tinycolor/tinycolor.dart';
import 'package:url_launcher/url_launcher.dart';

class PrivacyPolicyScreen extends StatefulWidget {
  @override
  _PrivacyPolicyScreenState createState() => _PrivacyPolicyScreenState();
}

class _PrivacyPolicyScreenState extends State<PrivacyPolicyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Navigator.canPop(context) ? const BackButton(
          key: ValueKey('ppBackButton'),
        ) : null,
        title: Text(
            GothaerLocalizations.of(context).translate('PRIVACY_POLICY_TITLE')),
      ),
      body: FutureBuilder<String>(
        future: rootBundle.loadString(
            'assets/privacypolicy/${GothaerLocalizations.of(context).locale.languageCode.toLowerCase()}_${GothaerLocalizations.of(context).locale.countryCode.toUpperCase()}_privacy_policy.md'),
        builder: (_, snapshot) {
          if (snapshot.hasData) {
            return Center(
              child: Material(
                elevation: 5,
                color: TinyColor(Theme.of(context).scaffoldBackgroundColor).brighten(5).color,
                child: SingleChildScrollView(
                  key: const ValueKey('ppScrollArea'),
                  child: Container(
                    padding: const EdgeInsets.all(16),
                    constraints: const BoxConstraints(maxWidth: 680),
                    child: MarkdownBody(
                      key: const ValueKey('ppMarkdown'),
                      onTapLink: (String text, String href, String title) async {
                        if (href != null) {
                          if (await canLaunch(href)) {
                            await launch(href);
                          }
                        }
                      },
                      data: snapshot.data,
                    ),
                  ),
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const Icon(Icons.warning_outlined),
                  Container(
                    padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
                    child: Text(GothaerLocalizations.of(context).translate('ERROR_LOADING_PRIVACY_POLICY')),
                  ),
                ],
              ),
            );
          }
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                const CircularProgressIndicator.adaptive(),
                Container(
                  padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
                  child: Text(GothaerLocalizations.of(context).translate('LOADING_LABEL')),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
