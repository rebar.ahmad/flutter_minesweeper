import 'package:flutter/material.dart';
import 'package:minesweeper/GameScreen/service.dart';
import 'package:minesweeper/SettingsScreen/widgets/FieldNumbersGrid.dart';
import 'package:minesweeper/app/constants/MediaHelper.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';
import 'package:minesweeper/app/model/AppInfo.dart';
import 'package:minesweeper/app/service.dart';
import 'package:minesweeper/SettingsScreen/screens/LanguageScreen.dart';
import 'package:minesweeper/SettingsScreen/widgets/BrightnessDialog.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({
    Key key,
    @required this.appInfo,
  }) : super(key: key);

  final AppInfo appInfo;

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  AppNotifier appService;

  num dropdownValue = 10;

  @override
  void initState() {
    super.initState();
    appService = Provider.of<AppNotifier>(context, listen: false);
  }

  void onChangeBrightness() {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) => BrightnessDialog(
        service: appService,
      ),
    );
  }

  void onChangeLanguage() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const LanguageScreen(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    String savedLocale;
    if (appService.locale != null)
      savedLocale = GothaerLocalizations.of(context).translate(
          '${appService.locale?.languageCode?.toUpperCase()}_${appService.locale?.countryCode?.toUpperCase()}_LABEL');

    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(
          key: ValueKey('settingsBackButton'),
        ),
        title:
            Text(GothaerLocalizations.of(context).translate('SETTINGS_LABEL')),
      ),
      body: Center(
        child: ConstrainedBox(
          constraints: MediaHelper.tabletConstraints,
          child: Stack(children: [
            Positioned.fill(
              bottom: 25,
              child: ListView(
                children: <Widget>[
                  ListTile(
                    key: const ValueKey('ssAppTheme'),
                    onTap: onChangeBrightness,
                    leading: Icon(appService.brightness.icon),
                    title: Text(
                      GothaerLocalizations.of(context)
                          .translate('APP_THEME_LABEL'),
                      softWrap: true,
                    ),
                    trailing: Text(
                      GothaerLocalizations.of(context).translate(
                        'APP_THEME_' +
                            appService.brightness.label.toUpperCase(),
                      ),
                      key: const ValueKey('themeIndicatorText'),
                    ),
                  ),
                  ListTile(
                    key: const ValueKey('ssAppLanguage'),
                    onTap: onChangeLanguage,
                    leading: const Icon(Icons.translate),
                    title: Text(
                      GothaerLocalizations.of(context)
                          .translate('APP_LANGUAGE_LABEL'),
                      softWrap: true,
                    ),
                    trailing:
                        appService.locale != null ? Text(savedLocale) : null,
                  ),
                  Consumer<GameNotifier>(
                    builder: (_, notifier, widget) => ListTile(
                      leading: SizedBox(
                        height: 25,
                        width: 25,
                        child: FieldNumbersGrid(
                          numberOfFields: notifier.numberOfFields,
                        ),
                      ),
                      title: Text(
                        GothaerLocalizations.of(context)
                            .translate('NUMBER_OF_FIELDS_LABEL'),
                        softWrap: true,
                      ),
                      trailing: DropdownButton<num>(
                        key: const ValueKey('ssNumberOfFields'),
                        value: notifier.numberOfFields,
                        icon: const Icon(Icons.arrow_drop_down),
                        onChanged: (num value) {
                          setState(() {
                            notifier.numberOfFields = value;
                          });
                        },
                        items: <num>[10, 15, 20]
                            .map<DropdownMenuItem<num>>((num value) {
                          return DropdownMenuItem<num>(
                            key: ValueKey('ssFields_$value'),
                            value: value,
                            child: Text('${value}x$value'),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 10,
              left: 10,
              right: 10,
              child: SafeArea(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Expanded(
                      child: Divider(thickness: 1.0,),
                    ),
                    Opacity(
                      opacity: .5,
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Text('v. ${widget.appInfo.appVersion}'),
                      ),
                    ),
                    const Expanded(
                      child: Divider(thickness: 1.0,),
                    ),
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
