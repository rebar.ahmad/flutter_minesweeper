import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:minesweeper/app/components/GothaerSheets.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';
import 'package:minesweeper/app/i18n/LanguageManager.dart';
import 'package:minesweeper/app/service.dart';
import 'package:minesweeper/SettingsScreen/widgets/LanguageTile.dart';
import 'package:provider/provider.dart';
import 'package:tinycolor/tinycolor.dart';

class LanguageScreen extends StatefulWidget {
  const LanguageScreen({Key key}) : super(key: key);

  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  List<LanguageTile> _languages = [];
  final TextEditingController _searchLanguagesController =
      TextEditingController();
  final FocusNode _searchLanguagesFocusNode = FocusNode();
  AppNotifier service;

  @override
  void initState() {
    super.initState();
    service = Provider.of<AppNotifier>(context, listen: false);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_searchLanguagesController.text.isEmpty) {
      _initLanguageList();
    }
  }

  void saveLanguage(Locale newLocale) {
    service.locale = newLocale;

    _initLanguageList();
    setState(() {
      _searchLanguagesFocusNode.unfocus();
      _searchLanguagesController.clear();
    });
  }

  void _checkoutLanguageChange(Locale lang) {
    String languageName = GothaerLocalizations.of(context).translate(
        '${lang.languageCode.toUpperCase()}_${lang.countryCode.toUpperCase()}_LABEL');

    GothaerSheets.showActionSheet(
      context,
      title: Text(
        GothaerLocalizations.of(context).translate('LANG_CHANGE_TITLE_PRE') +
            languageName +
            GothaerLocalizations.of(context)
                .translate('LANG_CHANGE_TITLE_POST'),
      ),
      actions: Theme.of(context).platform == TargetPlatform.iOS ||
              Theme.of(context).platform == TargetPlatform.macOS
          ? <Widget>[
              CupertinoActionSheetAction(
                key: const ValueKey('submitLanguage'),
                onPressed: () {
                  Navigator.of(context).pop();
                  saveLanguage(lang);
                },
                child: GothaerLocalizations.of(context)
                            .locale
                            .languageCode
                            .toLowerCase() ==
                        'de'
                    ? Text(languageName +
                        ' ' +
                        GothaerLocalizations.of(context)
                            .translate('LANG_CHANGE_USE'))
                    : Text(GothaerLocalizations.of(context)
                            .translate('LANG_CHANGE_USE') +
                        ' ' +
                        languageName),
              )
            ]
          : <Widget>[
              ListTile(
                key: const ValueKey('submitLanguage'),
                onTap: () {
                  Navigator.of(context).pop();
                  saveLanguage(lang);
                },
                title: GothaerLocalizations.of(context)
                            .locale
                            .languageCode
                            .toLowerCase() ==
                        'de'
                    ? Text(
                        languageName +
                            ' ' +
                            GothaerLocalizations.of(context)
                                .translate('LANG_CHANGE_USE'),
                        style: TextStyle(
                            fontSize:
                                MediaQuery.of(context).textScaleFactor * 20),
                      )
                    : Text(
                        GothaerLocalizations.of(context)
                                .translate('LANG_CHANGE_USE') +
                            ' ' +
                            languageName,
                        style: TextStyle(
                            fontSize:
                                MediaQuery.of(context).textScaleFactor * 20),
                      ),
              ),
            ],
    );
  }

  void _initLanguageList() {
    _languages = [];
    // ignore: avoid_function_literals_in_foreach_calls
    languageManager.supportedLanguagesCodes.forEach((lang) {
      String title = GothaerLocalizations.of(context).translate(
          '${lang.languageCode.toUpperCase()}_${lang.countryCode.toUpperCase()}_LABEL');

      _languages.add(
        LanguageTile(
          key: ValueKey('language_${lang.languageCode}_${lang.countryCode}'),
          onLanguageTap: () => _checkoutLanguageChange(lang),
          title: title,
          selected: lang == GothaerLocalizations.of(context).locale,
        ),
      );
    });
    _languages.sort((a, b) {
      return a.getLanguageName
          .toLowerCase()
          .compareTo(b.getLanguageName.toLowerCase());
    });
  }

  void _search(String query) {
    setState(() {
      if (query.isNotEmpty) {
        _initLanguageList();
        _languages = _languages.where((lang) {
          String language = lang.getLanguageName.toLowerCase();
          return language.startsWith(query.toLowerCase());
        }).toList();
      } else {
        _initLanguageList();
      }
    });
  }

  Widget _searchBar() {
    return Material(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      elevation: 3,
      color: TinyColor(Theme.of(context).primaryColor).brighten().color,
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              width: 40,
              padding: const EdgeInsets.only(left: 10),
              child: const BackButton(
                key: ValueKey('languageBackButton'),
                color: Colors.white,
              ),
            ),
            Expanded(
              child: TextField(
                key: const ValueKey('languageSearchBar'),
                cursorColor: Colors.white,
                controller: _searchLanguagesController,
                focusNode: _searchLanguagesFocusNode,
                style: const TextStyle(
                  color: Colors.white,
                ),
                onChanged: (String val) => _search(val),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(top: 15),
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  border: InputBorder.none,
                  suffixIcon: AnimatedSwitcher(
                    duration: const Duration(milliseconds: 250),
                    switchInCurve: Curves.easeInCubic,
                    switchOutCurve: Curves.easeOutCubic,
                    child: _searchLanguagesController.text.isNotEmpty
                        ? IconButton(
                            key: const ValueKey('clearLanguageSearch'),
                            padding: const EdgeInsets.only(right: 10),
                            onPressed: () {
                              _searchLanguagesController.clear();
                              _initLanguageList();
                              setState(() {});
                            },
                            icon: const Icon(Icons.close, color: Colors.white),
                          )
                        : const SizedBox(width: 10, height: 10,),
                  ),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.white.withOpacity(.5),
                  ),
                  hintStyle: TextStyle(
                    color: Colors.white.withOpacity(.5),
                  ),
                  prefixStyle: const TextStyle(
                    color: Colors.white,
                  ),
                  hintText:
                      GothaerLocalizations.of(context).translate('SEARCH_HINT'),
                ),
              ),
            )
          ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: kToolbarHeight * 1.2,
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: _searchBar(),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: ListView(
          children:
              (_languages.isEmpty && _searchLanguagesController.text.isNotEmpty
                  ? [
                      ListTile(
                        title: Text(
                          GothaerLocalizations.of(context)
                                  .translate('NO_RESULTS_FOR_LABEL') +
                              ': ' +
                              _searchLanguagesController.text,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ]
                  : _languages),
        ),
      ),
    );
  }
}
