import 'package:flutter/material.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';
import 'package:minesweeper/app/service.dart';
import 'package:minesweeper/SettingsScreen/widgets/BrightnessTiles.dart';

class BrightnessDialog extends StatefulWidget {
  const BrightnessDialog({
    Key key,
    @required this.service,
  }) : super(key: key);

  final AppNotifier service;

  @override
  _BrightnessDialogState createState() => _BrightnessDialogState();
}

class _BrightnessDialogState extends State<BrightnessDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      key: const ValueKey('brightnessDialog'),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      contentPadding: const EdgeInsets.all(0),
      content: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 25, right: 25),
              child: Text(
                GothaerLocalizations.of(context).translate('APP_THEME_LABEL'),
                style: const TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            const SizedBox(height: 10),
            BrightnessTiles(
              appBrightness: widget.service.brightness,
              onChanged: (value) => setState(() {
                widget.service.brightness = value;
              }),
            ),
          ],
        ),
      ),
      actionsPadding: const EdgeInsets.only(right: 15, top: 15, bottom: 10),
      actions: [
        FlatButton(
          key: const ValueKey('closeBrightnessButton'),
          child: Text(
            GothaerLocalizations.of(context).translate('CLOSE_LABEL'),
            style: const TextStyle(
              fontSize: 16,
            ),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ],
    );
  }
}