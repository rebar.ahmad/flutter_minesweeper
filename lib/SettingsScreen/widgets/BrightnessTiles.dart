import 'package:flutter/material.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';
import 'package:minesweeper/app/model/AppBrightness.dart';

class BrightnessTiles extends StatefulWidget {
  const BrightnessTiles({
    Key key,
    @required this.appBrightness,
    @required this.onChanged,
  }) : super(key: key);

  final AppBrightness appBrightness;
  final ValueChanged<AppBrightness> onChanged;

  @override
  _BrightnessTileState createState() => _BrightnessTileState();
}

class _BrightnessTileState extends State<BrightnessTiles> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        RadioListTile<AppBrightness>(
          key: const ValueKey('lightValue'),
          title: Text(
            GothaerLocalizations.of(context).translate(
              'APP_THEME_' + AppBrightness.light.label.toUpperCase(),
            ),
          ),
          secondary: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Icon(AppBrightness.light.icon),
          ),
          activeColor: Theme.of(context).accentColor,        
          value: AppBrightness.light,
          groupValue: widget.appBrightness,
          onChanged: (AppBrightness value) {
            setState(() {
              widget.onChanged(value);
            });
          },
        ),
        RadioListTile<AppBrightness>(
          key: const ValueKey('darkValue'),
          title: Text(
            GothaerLocalizations.of(context).translate(
              'APP_THEME_' + AppBrightness.dark.label.toUpperCase(),
            ),
          ),
          secondary: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Icon(AppBrightness.dark.icon),
          ),
          activeColor: Theme.of(context).accentColor,
          value: AppBrightness.dark,
          groupValue: widget.appBrightness,
          onChanged: (AppBrightness value) {
            setState(() {
              widget.onChanged(value);
            });
          },
        ),
        RadioListTile<AppBrightness>(
          key: const ValueKey('systemValue'),
          title: Text(
            GothaerLocalizations.of(context).translate(
              'APP_THEME_' + AppBrightness.system.label.toUpperCase(),
            ),
          ),
          secondary: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Icon(AppBrightness.system.icon),
          ),
          activeColor: Theme.of(context).accentColor,
          value: AppBrightness.system,
          groupValue: widget.appBrightness,
          onChanged: (AppBrightness value) {
            setState(() {
              widget.onChanged(value);
            });
          },
        ),
      ],
    );
  }
}
