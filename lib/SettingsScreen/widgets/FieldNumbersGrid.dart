import 'package:flutter/material.dart';

class FieldNumbersGrid extends StatefulWidget {
  const FieldNumbersGrid({
    Key key,
    this.numberOfFields = 10,
  }) : super(key: key);

  final num numberOfFields;

  @override
  _FieldNumbersGridState createState() => _FieldNumbersGridState();
}

class _FieldNumbersGridState extends State<FieldNumbersGrid> {
  @override
  Widget build(BuildContext context) {
    final int numberOfItems = widget.numberOfFields == 10 ? 4 : widget
        .numberOfFields == 15 ? 9 : widget.numberOfFields == 20 ? 16 : 4;
    final int crossAxisCount = widget.numberOfFields == 10 ? 2 : widget
        .numberOfFields == 15 ? 3 : widget.numberOfFields == 20 ? 4 : 2;

    final borderColor = Theme
        .of(context)
        .brightness == Brightness.light
        ? Colors.black54
        : Colors.white;

    return GridView.builder(
      itemCount: numberOfItems,
      itemBuilder: (_, i) =>
          Container(
            padding: const EdgeInsets.all(2),
            width: 2,
            height: 2,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(2.5),
              border: Border.all(color: borderColor, width: 1.0),
            ),
          ),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: crossAxisCount,
        mainAxisSpacing: 2,
        crossAxisSpacing: 2,
      ),
    );
  }
}
