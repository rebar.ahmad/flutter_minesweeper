import 'package:flutter/material.dart';

/// A Language List Element showing an available app language
class LanguageTile extends StatefulWidget {
  final VoidCallback onLanguageTap;
  final String title;
  final bool selected;

  String get getLanguageName => this.title;

  LanguageTile({
    Key key,
    @required this.onLanguageTap,
    @required this.title,
    this.selected,
  }) : super(key: key);

  @override
  _LanguageTileState createState() => _LanguageTileState();
}

class _LanguageTileState extends State<LanguageTile> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: widget.onLanguageTap,
      leading: Icon(
        Icons.language,
        color: widget.selected ? Theme.of(context).accentColor : Colors.grey,
      ),
      title: Text(
        widget.title,
        style: TextStyle(
          fontSize: 18,
          fontWeight: widget.selected ? FontWeight.bold : FontWeight.normal,
        ),
      ),
    );
  }
}
