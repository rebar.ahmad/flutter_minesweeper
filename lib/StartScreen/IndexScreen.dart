import 'package:flutter/material.dart';
import 'package:minesweeper/GameScreen/service.dart';
import 'package:minesweeper/SettingsScreen/SettingsScreen.dart';
import 'package:minesweeper/StartScreen/StartScreen.dart';
import 'package:minesweeper/app/constants/MediaHelper.dart';
import 'package:minesweeper/app/model/AppInfo.dart';
import 'package:minesweeper/app/plugins/package_info/package_info.dart';
import 'package:provider/provider.dart';

class IndexScreen extends StatefulWidget {
  @override
  _IndexScreenState createState() => _IndexScreenState();
}

class _IndexScreenState extends State<IndexScreen> {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  AppInfo appInfo;

  @override
  void initState() {
    super.initState();
    initAppInfo();
  }

  Future<void> initAppInfo() async {
    appInfo = await PackageInfoUtils.fromPlatform();
    setState(() {});
  }

  Future<void> toSettings() async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => SettingsScreen(appInfo: appInfo),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, constraints) => Consumer<GameNotifier>(
        builder: (_, notifier, widget) => Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            automaticallyImplyLeading: !notifier.startedGame ? false : true,
            leading: notifier.startedGame
                ? IconButton(
                    icon: const Icon(Icons.home_filled),
                    onPressed: () => setState(() =>
                    notifier.startedGame = !notifier.startedGame),
                  )
                : null,
            backgroundColor: Colors.transparent,
            elevation: 0,
            textTheme: Theme.of(context).textTheme.copyWith(
                  headline6: Theme.of(context).textTheme.headline6.copyWith(
                        color: Theme.of(context).brightness == Brightness.light
                            ? Colors.black54
                            : Colors.white,
                      ),
                ),
            iconTheme: Theme.of(context).iconTheme.copyWith(
                  color: Theme.of(context).brightness == Brightness.light
                      ? Colors.black54
                      : Colors.white,
                ),
            actions: [
              IconButton(
                key: const ValueKey('settingsButton'),
                icon: const Icon(Icons.settings),
                onPressed: () => constraints.maxWidth <= kMobileBreakpoint
                    ? toSettings()
                    : scaffoldKey.currentState.openEndDrawer(),
              ),
            ],
          ),
          endDrawer: constraints.maxWidth > kMobileBreakpoint
              ? Drawer(
                  child: SettingsScreen(appInfo: appInfo),
                )
              : null,
          body: StartScreen(),
        ),
      ),
    );
  }
}
