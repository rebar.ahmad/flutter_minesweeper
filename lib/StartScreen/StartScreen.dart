import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:minesweeper/GameScreen/GameScreen.dart';
import 'package:minesweeper/GameScreen/service.dart';
import 'package:minesweeper/PrivacyPolicyScreen/PrivacyPolicyScreen.dart';
import 'package:minesweeper/StartScreen/widgets/Controls.dart';
import 'package:minesweeper/app/constants/MediaHelper.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';
import 'package:provider/provider.dart';

class StartScreen extends StatefulWidget {
  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  GameNotifier gameService;

  @override
  void initState() {
    super.initState();
    gameService = Provider.of<GameNotifier>(context, listen: false);
  }

  void toPrivacyPolicy() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => PrivacyPolicyScreen(),
      ),
    );
  }

  Future<void> onGameStarted() async {
    setState(() {
      gameService.startedGame = !gameService.startedGame;
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, constraints) => Scaffold(
        body: SafeArea(
          bottom: false,
          child: AnimatedSwitcher(
            duration: const Duration(milliseconds: 250),
            switchInCurve: Curves.easeInCubic,
            switchOutCurve: Curves.easeOutCubic,
            child: gameService.startedGame
                ? const GameScreen()
                : Stack(
                    children: [
                      Positioned.fill(
                        child: Opacity(
                          opacity: .5,
                          child: Image.asset(
                            'assets/images/startscreen_bg.png',
                            fit: constraints.maxWidth < kDesktopBreakpoint
                                ? BoxFit.cover
                                : BoxFit.contain,
                          ),
                        ),
                      ),
                      Positioned.fill(
                        child: Center(
                          child: SingleChildScrollView(
                            physics: const ClampingScrollPhysics(),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                FittedBox(
                                  fit: BoxFit.contain,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 50.0),
                                    child: AutoSizeText(
                                      'DigitalSweeper',
                                      maxLines: 1,
                                      textAlign: TextAlign.center,
                                      style: const TextStyle().copyWith(
                                        shadows: [
                                          const Shadow(
                                            color: Colors.black,
                                            blurRadius: 5,
                                          ),
                                        ],
                                        fontSize: 80,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 100,
                                ),
                                Controls(
                                  key: const ValueKey('startControls'),
                                  onStartGame: onGameStarted,
                                  startGameLabel:
                                      GothaerLocalizations.of(context)
                                          .translate('START_GAME_LABEL'),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 15,
                        right: 0,
                        left: 0,
                        child: Center(
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            physics: const ClampingScrollPhysics(),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                FlatButton(
                                  key: const ValueKey('privacyPolicyButton'),
                                  onPressed: toPrivacyPolicy,
                                  child: Text(GothaerLocalizations.of(context)
                                      .translate('PRIVACY_POLICY_TITLE')),
                                ),
                                Container(
                                  height: 20,
                                  child: const VerticalDivider(thickness: 1.0,),
                                ),
                                Container(
                                  padding: const EdgeInsets.only(left: 7.5),
                                  child: const Text(
                                    '© Rebar Ahmad',
                                    key: const ValueKey('copyrightLabel'),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
