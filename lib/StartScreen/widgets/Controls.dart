import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:minesweeper/app/constants/MediaHelper.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';

class Controls extends StatelessWidget {
  const Controls({
    Key key,
    @required this.onStartGame,
    @required this.startGameLabel,
    this.onExitGame,
    this.exitGameLabel,
  }) : super(key: key);

  final VoidCallback onStartGame;
  final VoidCallback onExitGame;

  final String startGameLabel;
  final String exitGameLabel;

  @override
  Widget build(BuildContext context) {
    final buttonColor = Theme.of(context).primaryColor;
    final textColor = Colors.white;
    final buttonPadding =
        const EdgeInsets.symmetric(horizontal: 25, vertical: 15);
    final platform = Theme.of(context).platform;
    final isDesktop = platform == TargetPlatform.windows ||
        platform == TargetPlatform.macOS ||
        platform == TargetPlatform.linux;
    final isMobile = platform == TargetPlatform.iOS ||
        platform == TargetPlatform.android ||
        platform == TargetPlatform.fuchsia;

    if (kIsWeb || isMobile) {
      return RaisedButton(
        key: const ValueKey('startGameButton'),
        color: buttonColor,
        padding: buttonPadding,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
        onPressed: onStartGame,
        child: AutoSizeText(
          startGameLabel,
          maxLines: 1,
          style: const TextStyle().copyWith(
            fontSize: 28,
            color: textColor,
          ),
        ),
      );
    }

    if (isDesktop) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            constraints: MediaHelper.mobileConstraints.copyWith(minWidth: 250),
            child: RaisedButton(
              key: const ValueKey('startGameButton'),
              color: buttonColor,
              padding: buttonPadding,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50),
              ),
              onPressed: onStartGame,
              child: AutoSizeText(
                startGameLabel,
                maxLines: 1,
                style: const TextStyle().copyWith(
                  fontSize: 28,
                  color: textColor,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            constraints: MediaHelper.mobileConstraints.copyWith(minWidth: 250),
            child: RaisedButton(
              key: const ValueKey('exitGameButton'),
              color: buttonColor,
              padding: buttonPadding,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50),
              ),
              onPressed:
                  onExitGame ?? () => SystemNavigator.pop(animated: true),
              child: AutoSizeText(
                exitGameLabel ??
                    GothaerLocalizations.of(context)
                        .translate('EXIT_GAME_LABEL'),
                maxLines: 1,
                style: const TextStyle().copyWith(
                  fontSize: 24,
                  color: textColor,
                ),
              ),
            ),
          )
        ],
      );
    }

    return RaisedButton(
      key: const ValueKey('startGameButton'),
      color: buttonColor,
      padding: buttonPadding,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50),
      ),
      onPressed: onStartGame,
      child: AutoSizeText(
        startGameLabel,
        maxLines: 1,
        style: const TextStyle().copyWith(
          fontSize: 28,
          color: textColor,
        ),
      ),
    );
  }
}
