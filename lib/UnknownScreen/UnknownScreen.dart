import 'package:flutter/material.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';
import 'package:minesweeper/StartScreen/StartScreen.dart';

class UnknownScreen extends StatelessWidget {
  const UnknownScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5ECCA),
      body: Stack(
        fit: StackFit.expand,
        alignment: Alignment.center,
        children: [
          Positioned(
            bottom: 0,
            child: Image.asset(
              'assets/images/unknownscreen_bg.jpg',
              fit: BoxFit.cover,
              height: MediaQuery.of(context).size.height / 2,
            ),
          ),
          SingleChildScrollView(
            physics: const ClampingScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Text(
                  '404',
                  textScaleFactor: 10,
                  style: TextStyle(
                    color: Color(0xFFff4c60),
                  ),
                ),
                Text(
                  GothaerLocalizations.of(context)
                      .translate('UNKNOWNSCREEN_TITLE'),
                  textAlign: TextAlign.center,
                  textScaleFactor: 2,
                  style: const TextStyle(
                    color: Color(0xFFff4c60),
                  ),
                ),
                Text(
                  GothaerLocalizations.of(context)
                      .translate('UNKNOWNSCREEN_SUBTITLE'),
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: Color(0xFFff4c60),
                  ),
                ),
                const SizedBox(height: 20),
                MaterialButton(
                  elevation: 0,
                  minWidth: 150,
                  onPressed: () => Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (_) => StartScreen(),
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                    side: const BorderSide(
                      width: 1.0,
                      color: Color(0xFFff4c60),
                    ),
                  ),
                  child: Text(
                    GothaerLocalizations.of(context)
                        .translate('UNKNOWNSCREEN_BUTTON_LABEL'),
                    style: const TextStyle(
                      color: Color(0xFFff4c60),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
