import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:minesweeper/GameScreen/service.dart';
import 'package:minesweeper/StartScreen/IndexScreen.dart';
import 'package:minesweeper/app/i18n/LanguageManager.dart';
import 'package:minesweeper/app/routes.dart';
import 'package:minesweeper/app/service.dart';
import 'package:minesweeper/app/theme.dart';
import 'package:minesweeper/UnknownScreen/UnknownScreen.dart';
import 'package:provider/provider.dart';

class MineSweeperApp extends StatefulWidget {
  const MineSweeperApp({
    this.service,
    this.gameService,
  });

  final AppNotifier service;
  final GameNotifier gameService;

  @override
  _MineSweeperAppState createState() => _MineSweeperAppState();
}

class _MineSweeperAppState extends State<MineSweeperApp> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AppNotifier>.value(
          value: widget.service,
        ),
        ChangeNotifierProvider<GameNotifier>.value(
          value: widget.gameService,
        ),
      ],
      child: Consumer<AppNotifier>(
        builder: (_, notifier, child) => MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'DigitalSweeper',
          themeMode: widget.service.brightness.mode,
          theme: AppTheme.light,
          darkTheme: AppTheme.dark,
          home: IndexScreen(),
          routes: routes,
          onUnknownRoute: (settings) => MaterialPageRoute(
            builder: (_) => const UnknownScreen(),
          ),
          supportedLocales: languageManager.supportedLocales(),
          localizationsDelegates: [
            widget.service.localizationsDelegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            DefaultCupertinoLocalizations.delegate
          ],
          localeResolutionCallback:
              (Locale locale, Iterable<Locale> supportedLocales) {
            if (locale == null) {
              if (widget.service.settingSaved('user.language')) {
                return widget.service.locale;
              }
              return supportedLocales.first;
            }
            for (Locale supportedLocale in supportedLocales) {
              if (supportedLocale.languageCode == locale.languageCode ||
                  supportedLocale.countryCode == locale.countryCode) {
                if (widget.service.settingSaved('user.language')) {
                  return widget.service.locale;
                }
                return supportedLocale;
              }
            }
            if (widget.service.settingSaved('user.language')) {
              return widget.service.locale;
            }
            return supportedLocales.first;
          },
        ),
      ),
    );
  }
}
