import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';

/// Provides templates of action sheets with which one can create dynamic sheets
class GothaerSheets {
  /// Default constraints
  static BoxConstraints constraints = const BoxConstraints(
    maxWidth: 600,
    maxHeight: 400,
  );

  ///
  /// Bottom-Sheet
  ///
  /// Shows a Cupertino Action-Sheet (on iOS) or a Material Bottom-Sheet which pops up from bottom
  static void showActionSheet(
    BuildContext context, {
    Widget title,
    Widget message,
    List<Widget> actions,
    bool showCloseButton = true,
  }) async {
    if (Theme.of(context).platform == TargetPlatform.iOS ||
        Theme.of(context).platform == TargetPlatform.macOS) {
      await showCupertinoModalPopup(
        context: context,
        builder: (context) {
          return ConstrainedBox(
            constraints: constraints,
            child: CupertinoActionSheet(
              key: const ValueKey('actionSheet'),
              title: title ?? null,
              message: message ?? null,
              cancelButton: showCloseButton
                  ? CupertinoActionSheetAction(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text(
                        GothaerLocalizations.of(context).translate('CLOSE_LABEL'),
                      ),
                    )
                  : null,
              actions: actions ?? [],
            ),
          );
        },
      );
    } else {
      await showModalBottomSheet(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15),
          ),
        ),
        context: context,
        builder: (context) {
          return Container(
            key: const ValueKey('actionSheet'),
            constraints: constraints,
            padding: const EdgeInsets.all(14),
            child: ListView(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              children: <Widget>[
                if (title != null)
                  ListTile(
                    title: title,
                  )
                else
                  Container(),
                if (message != null) ListTile(title: message) else Container(),
                if (actions != null) const Divider() else Container(),
                ...actions ?? [Container()],
                if (showCloseButton)
                  ListTile(
                    onTap: () => Navigator.of(context).pop(),
                    leading: const Icon(Icons.arrow_downward),
                    title: Text(
                      GothaerLocalizations.of(context).translate('CLOSE_LABEL'),
                    ),
                  )
                else
                  Container()
              ],
            ),
          );
        },
      );
    }
  }
}
