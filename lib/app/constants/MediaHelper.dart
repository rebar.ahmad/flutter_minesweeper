import 'package:flutter/material.dart';

final kDesktopBreakpoint = 1024.0;
final kTabletBreakpoint = 860.0;
final kPhabletBreakpoint = 720.0;
final kMobileBreakpoint = 420.0;

class MediaHelper {
  static final BoxConstraints desktopConstraints = BoxConstraints(
    maxWidth: kDesktopBreakpoint,
  );
  static final BoxConstraints tabletConstraints = BoxConstraints(
    maxWidth: kTabletBreakpoint,
  );
  static final BoxConstraints phabletConstraints = BoxConstraints(
    maxWidth: kPhabletBreakpoint,
  );
  static final BoxConstraints mobileConstraints = BoxConstraints(
    maxWidth: kMobileBreakpoint,
  );
}