import 'package:flutter/material.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';
import 'package:minesweeper/app/i18n/LanguageManager.dart';

class GothaerDelegate extends LocalizationsDelegate<GothaerLocalizations> {
  final Locale newLocale;

  const GothaerDelegate({this.newLocale});

  @override
  bool isSupported(Locale locale) => languageManager.supportedLanguagesCodes.contains(locale);

  @override
  Future<GothaerLocalizations> load(Locale locale) async {
    GothaerLocalizations localizations = new GothaerLocalizations(newLocale ?? locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(GothaerDelegate old) => true;
}