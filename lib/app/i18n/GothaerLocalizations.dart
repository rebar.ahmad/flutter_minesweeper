import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:minesweeper/app/i18n/GothaerDelegate.dart';

class GothaerLocalizations {
  GothaerLocalizations(this.locale);

  final Locale locale;

  static GothaerLocalizations of(BuildContext context) {
    return Localizations.of<GothaerLocalizations>(
        context, GothaerLocalizations);
  }

  static const LocalizationsDelegate<GothaerLocalizations> delegate =
      GothaerDelegate();

  Map<String, String> _sentences;

  Future<bool> loadAppSentences() async {
    String data = await rootBundle.loadString(
        'assets/internationalization/${this.locale.languageCode.toLowerCase()}_${this.locale.countryCode.toUpperCase()}.json');
    Map<String, dynamic> _result = json.decode(data) as Map<String, dynamic>;

    this._sentences = {};
    _result.forEach((String key, dynamic value) {
      this._sentences[key] = value.toString();
    });

    return true;
  }

  Future<bool> load() async {
    await loadAppSentences();
    return true;
  }

  String translate(String key) => this._sentences[key];
}
