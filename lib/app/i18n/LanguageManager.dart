import 'dart:ui';

class LanguageManager {
  static final LanguageManager _languageManager = LanguageManager._internal();

  factory LanguageManager() {
    return _languageManager;
  }

  LanguageManager._internal();

  final List<Locale> supportedLanguagesCodes = [
    const Locale('en', 'GB'),
    const Locale('en', 'US'),
    const Locale('de', 'DE'),
  ];

  Iterable<Locale> supportedLocales() => supportedLanguagesCodes.map<Locale>((language) => language);
}

LanguageManager languageManager = LanguageManager();
