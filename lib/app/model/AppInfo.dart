class AppInfo {
  AppInfo({
    this.appName = 'DigitalSweeper',
    this.appVersion = '1.1.2',
    this.versionCode = '4',
    this.packageName = 'com.ahmadre.minesweeper',
  });

  final String appName;
  final String appVersion;
  final String versionCode;
  final String packageName;
}