import 'package:minesweeper/app/model/AppInfo.dart';
import 'package:package_info/package_info.dart';

class PackageInfoUtils {
  PackageInfoUtils._();

  static Future<AppInfo> fromPlatform() async {
    PackageInfo info = await PackageInfo.fromPlatform();
    return AppInfo(
      appName: info.appName,
      appVersion: info.version,
      versionCode: info.buildNumber,
      packageName: info.packageName,
    );
  }
}