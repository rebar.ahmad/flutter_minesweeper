import 'package:minesweeper/app/model/AppInfo.dart';

class PackageInfoUtils {
  PackageInfoUtils._();

  static Future<AppInfo> fromPlatform() async {
    return Future.value(AppInfo());
  }
}
