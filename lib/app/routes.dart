import 'package:flutter/material.dart';
import 'package:minesweeper/privacypolicyscreen/PrivacyPolicyScreen.dart';

final Map<String, Widget Function(BuildContext)> routes = {
  '/privacypolicy': (context) => PrivacyPolicyScreen(),
};