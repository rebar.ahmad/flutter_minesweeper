import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:minesweeper/app/i18n/GothaerDelegate.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';
import 'package:minesweeper/app/model/AppBrightness.dart';
import 'package:path_provider/path_provider.dart';

class AppNotifier extends ChangeNotifier {
  static Box box;

  AppBrightness _brightness;

  LocalizationsDelegate<GothaerLocalizations> _localizationsDelegate;

  Locale _locale;

  bool settingSaved(String key) => box.get(key) != null ? true : false;

  Locale get locale => _locale;

  set locale(Locale value) {
    if (_locale == value) {
      return;
    }
    _locale = value;
    _localizationsDelegate = GothaerDelegate(newLocale: _locale);
    box.put('user.language', _locale.toLanguageTag());
    notifyListeners();
  }

  LocalizationsDelegate<GothaerLocalizations> get localizationsDelegate =>
      _localizationsDelegate;

  set localizationsDelegate(LocalizationsDelegate<GothaerLocalizations> value) {
    if (_localizationsDelegate == value) {
      return;
    }
    _localizationsDelegate = value;
  }

  AppBrightness get brightness => _brightness;

  set brightness(AppBrightness value) {
    if (_brightness == value) {
      return;
    }
    _brightness = value;
    box.put('app.brightness', _brightness.value);
    notifyListeners();
  }

  static Future<AppNotifier> init() async {
    if (!kIsWeb) Hive.init((await getApplicationDocumentsDirectory()).path);
    if (!Hive.isBoxOpen('DSBox')) {
      box = await Hive.openBox('DSBox');
    } else {
      box = Hive.box('DSBox');
    }

    final brightness =
        AppBrightness.from(await box.get('app.brightness') as String);

    final language = await box.get('user.language');

    final locale = language != null
        ? Locale.fromSubtags(
            languageCode: (language as String).split('-')[0],
            countryCode: (language as String).split('-')[1])
        : null;
    final localizationsDelegate = GothaerLocalizations.delegate;

    return AppNotifier._(
      brightness,
      locale,
      localizationsDelegate,
    );
  }

  AppNotifier._(
    AppBrightness brightness,
    Locale locale,
    LocalizationsDelegate<GothaerLocalizations> localizationsDelegate,
  )   : _brightness = brightness,
        _locale = locale,
        _localizationsDelegate = localizationsDelegate;
}
