import 'package:flutter/material.dart';

class AppTheme {
  static final light = ThemeData(
    brightness: Brightness.light,
    primaryColor: const Color(0xFF00718F),
    accentColor: const Color(0xFF5E9D1B),
    buttonColor: const Color(0xFFED706C),
    fontFamily: 'FiraSans-Regular',
  );

  static final dark = ThemeData(
    brightness: Brightness.dark,
    primaryColor: const Color(0xFF00718F),
    accentColor: const Color(0xFF467514),
    buttonColor: const Color(0xFF991712),
    fontFamily: 'FiraSans-Regular',
  );
}
