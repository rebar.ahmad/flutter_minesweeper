import 'package:desktop_window/desktop_window.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:minesweeper/GameScreen/service.dart';
import 'package:minesweeper/app/app.dart';
import 'package:minesweeper/app/service.dart';
import 'package:universal_io/io.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final service = await AppNotifier.init();
  final gameService = await GameNotifier.init();

  if (!kIsWeb) {
    if (Platform.isWindows || Platform.isLinux || Platform.isMacOS) {
      await DesktopWindow.setMinWindowSize(const Size(720, 480));
    }
  }

  runApp(MineSweeperApp(
    service: service,
    gameService: gameService,
  ));
}
