// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:desktop_window/desktop_window.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:minesweeper/app/i18n/GothaerLocalizations.dart';
import 'package:minesweeper/app/model/AppBrightness.dart';
import 'package:mockito/mockito.dart';

class MockHiveInterface extends Mock implements HiveInterface {}

class MockHiveBox extends Mock implements Box {}

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  MockHiveInterface hiveInterface;
  MockHiveBox box;

  setUp(() {
    hiveInterface = MockHiveInterface();
    box = MockHiveBox();
  });

  group('App Initialization', () {
    test('should load user translations', () async {
      final localization = GothaerLocalizations(const Locale('en', 'GB'));
      final localsLoaded = await localization.loadAppSentences();

      expect(localsLoaded, true);
      expect(localization.translate('OK_LABEL'), 'OK');
    });

    test('should cache data', () async {
      when(hiveInterface.openBox(any)).thenAnswer((_) async => box);
      expect(box, isNotNull);

      await box.put('app.brightness', AppBrightness.light.value);
      box.get('app.brightness');
      when(box.get('app.brightness')).thenAnswer((_) => 'light');

      verify(box.put('app.brightness', AppBrightness.light.value)).called(1);
      verify(box.get('app.brightness'));
    });

    test('[desktop]: should set min window size', () async {
      MethodCall call;

      MethodChannel channel = const MethodChannel('desktop_window');

      channel.setMockMethodCallHandler((MethodCall methodCall) async {
        call = methodCall;
      });

      final size = const Size(720, 480);
      await DesktopWindow.setMinWindowSize(size);

      expect(call, isNotNull);
      expect(call.method, equals(MethodCall('setMinWindowSize', {'width': size.width, 'height': size.height}).method));
      expect(call.arguments, equals(MethodCall('setMinWindowSize', {'width': size.width, 'height': size.height}).arguments));

      channel.setMockMethodCallHandler(null);
    });
  });
}
