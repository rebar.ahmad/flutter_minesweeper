// Imports the Flutter Driver API.
import 'dart:io';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

/// flutter drive --target=test_driver/app.dart
/// macos: flutter drive --target=test_driver/app.dart -d macos
/// Browser: ./test_driver/chromedriver_87 --port=4444 oder: safaridriver --port=4444
/// flutter drive --target=test_driver/app.dart -d web-server --release --browser-dimension 720,480 --browser-name=safari
void main() {
  group('DigitalSweeper App', () {
    // First, define the Finders and use them to locate widgets from the
    // test suite. Note: the Strings provided to the `byValueKey` method must
    // be the same as the Strings we used for the Keys in step 1.
    final privacyPolicyButton = find.byValueKey('privacyPolicyButton');
    final copyrightLabel = find.byValueKey('copyrightLabel');
    final settingsButton = find.byValueKey('settingsButton');

    FlutterDriver driver;

    final screenshotDirectory =
        'test_driver/[${DateTime.now().toLocal().toString().replaceAll(' ', '_')}]_screenshots';

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
      await Directory(screenshotDirectory).create();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        await driver.close();
      }
    });

    Future<bool> isPresent(SerializableFinder byValueKey,
        {Duration timeout = const Duration(seconds: 1)}) async {
      try {
        await driver.waitFor(byValueKey, timeout: timeout);
        return true;
      } catch (exception) {
        return false;
      }
    }

    Future<void> takeScreenshot(String fileName) async {
      final List<int> pixels = await driver.screenshot();
      final File file = new File('$screenshotDirectory/$fileName');
      await file.writeAsBytes(pixels);
    }

    test('- Render start screen controls', () async {
      await Future.delayed(const Duration(seconds: 5));
      await driver.waitFor(find.byValueKey('startControls'));
      await takeScreenshot('start_screen.png');
    });

    test('- View the privacy policy', () async {
      if (await isPresent(privacyPolicyButton)) {
        await driver.tap(privacyPolicyButton);
      }

      await driver.waitFor(find.byValueKey('ppMarkdown'),
          timeout: const Duration(seconds: 5));
      await driver.scroll(
          find.byValueKey('ppScrollArea'), 0, -300, const Duration(seconds: 1));

      await takeScreenshot('privacy_policy.png');

      await driver.tap(find.byValueKey('ppBackButton'));

      if (await isPresent(privacyPolicyButton)) {
        expect(await driver.getText(copyrightLabel), '© Rebar Ahmad');
      }
    });

    test('- Change App Theme', () async {
      if (await isPresent(settingsButton)) {
        await driver.tap(settingsButton);
      }

      await driver.waitFor(find.byValueKey('ssAppTheme'),
          timeout: const Duration(seconds: 2));
      await driver.tap(find.byValueKey('ssAppTheme'));

      if (await isPresent(find.byValueKey('brightnessDialog'))) {
        await driver.tap(find.byValueKey('lightValue'));
        await Future.delayed(const Duration(seconds: 2));
        await takeScreenshot('light_mode.png');
        await driver.tap(find.byValueKey('darkValue'));
        await Future.delayed(const Duration(seconds: 2));
        await takeScreenshot('dark_mode.png');
        await driver.tap(find.byValueKey('systemValue'));
        await Future.delayed(const Duration(seconds: 2));
        await takeScreenshot('system_mode.png');
        await driver.tap(find.byValueKey('closeBrightnessButton'));

        await driver.waitForAbsent(find.byValueKey('brightnessDialog'));
        expect(await driver.getText(find.byValueKey('themeIndicatorText')),
            isNotNull);
      }
    });

    test('- Change App Language', () async {
      if (await isPresent(find.byValueKey('ssAppLanguage'))) {
        await driver.tap(find.byValueKey('ssAppLanguage'));
      }

      if (await isPresent(find.byValueKey('language_en_GB'))) {
        await driver.tap(find.byValueKey('language_en_GB'));
      }

      await driver.waitFor(find.byValueKey('actionSheet'),
          timeout: const Duration(seconds: 2));
      await driver.tap(find.byValueKey('submitLanguage'));

      await Future.delayed(const Duration(seconds: 2));
      await takeScreenshot('en_GB.png');

      if (await isPresent(find.byValueKey('language_en_US'))) {
        await driver.tap(find.byValueKey('language_en_US'));
      }

      await driver.waitFor(find.byValueKey('actionSheet'),
          timeout: const Duration(seconds: 2));
      await driver.tap(find.byValueKey('submitLanguage'));

      await Future.delayed(const Duration(seconds: 2));
      await takeScreenshot('en_US.png');

      if (await isPresent(find.byValueKey('language_de_DE'))) {
        await driver.tap(find.byValueKey('language_de_DE'));
      }

      await driver.waitFor(find.byValueKey('actionSheet'),
          timeout: const Duration(seconds: 2));
      await driver.tap(find.byValueKey('submitLanguage'));

      await Future.delayed(const Duration(seconds: 2));
      await takeScreenshot('de_DE.png');

      if (await isPresent(find.byValueKey('languageSearchBar'))) {
        await driver.tap(find.byValueKey('languageSearchBar'));
        await driver.enterText('Deut');
        await driver.waitFor(find.text('Deut'));
        await takeScreenshot('search_language.png');

        await driver.waitFor(find.byValueKey('clearLanguageSearch'));
        await driver.tap(find.byValueKey('clearLanguageSearch'));
        expect(await driver.getText(find.byValueKey('languageSearchBar')), '');

        await driver.tap(find.byValueKey('languageBackButton'));
      }
    });

    test('- Change number of board fields', () async {
      await driver.waitFor(find.byValueKey('ssNumberOfFields'));
      await driver.tap(find.byValueKey('ssNumberOfFields'));
      await driver.waitFor(find.byValueKey('ssFields_15'));
      await driver.tap(find.byValueKey('ssFields_15'));

      await Future.delayed(const Duration(seconds: 1));
      await takeScreenshot('fields_15.png');

      await driver.waitFor(find.byValueKey('ssNumberOfFields'));
      await driver.tap(find.byValueKey('ssNumberOfFields'));
      await driver.waitFor(find.byValueKey('ssFields_20'));
      await driver.tap(find.byValueKey('ssFields_20'));

      await Future.delayed(const Duration(seconds: 1));
      await takeScreenshot('fields_20.png');

      await driver.waitFor(find.byValueKey('ssNumberOfFields'));
      await driver.tap(find.byValueKey('ssNumberOfFields'));
      await driver.waitFor(find.byValueKey('ssFields_10'));
      await driver.tap(find.byValueKey('ssFields_10'));

      await Future.delayed(const Duration(seconds: 1));
      await takeScreenshot('fields_10.png');

      await driver.waitFor(find.byValueKey('settingsBackButton'));
      await driver.tap(find.byValueKey('settingsBackButton'));

      if (await isPresent(privacyPolicyButton)) {
        expect(await driver.getText(copyrightLabel), '© Rebar Ahmad');
      }
    });

    test('- Idle Status is displayed correctly', () async {
      await driver.waitFor(find.byValueKey('startGameButton'));
      await driver.tap(find.byValueKey('startGameButton'));

      await driver.waitFor(find.byValueKey('statusDisplay'));
      await driver.waitFor(find.text('10'));
      await driver.waitFor(find.text('0'));
      await takeScreenshot('game_board.png');
    });

    test('- 10x10 cells are initialized', () async {
      await driver.waitFor(find.byValueKey('gameBoard'));

      await driver.waitFor(find.byValueKey('cell_0_0'));
      await driver.waitFor(find.byValueKey('cell_9_9'));
    });

    test('- onCellTap is working properly', () async {
      /// boolean at the end defines if the cell revealed status changed
      await driver.waitFor(find.byValueKey('cellRevealed_0_0_false'));

      await driver.tap(find.byValueKey('cell_0_0'));

      await Future.delayed(const Duration(seconds: 1));

      if (await isPresent(find.byValueKey('lostDialog'))) {
        await takeScreenshot('lost_on_cell_revealed.png');
        await driver.tap(find.byValueKey('lostDialog'));
      } else {
        await driver.waitFor(find.byValueKey('cellRevealed_0_0_true'));
        await takeScreenshot('cell_revealed.png');
        await driver.tap(find.byValueKey('resetGameButton'));
      }
      await Future.delayed(const Duration(seconds: 1));
    });

    test('- onCellLongPress is working properly', () async {
      /// Simulate long press with scroll
      await driver.scroll(
          find.byValueKey('cell_9_9'), 0, 0, const Duration(seconds: 2));

      await Future.delayed(const Duration(seconds: 1));
      await takeScreenshot('long_press.png');

      await driver.waitFor(find.byValueKey('cellFlagged_9_9_true'));
      await driver.tap(find.byValueKey('resetGameButton'));
    });

    test('- Flagged fields do not react to tap', () async {
      await driver.scroll(
          find.byValueKey('cell_0_5'), 0, 0, const Duration(seconds: 2));

      await Future.delayed(const Duration(seconds: 1));

      await driver.tap(find.byValueKey('cell_0_5'));

      await driver.waitFor(find.byValueKey('cellFlagged_0_5_true'));
      await takeScreenshot('long_press_not_reacting_to_tap.png');

      await driver.tap(find.byValueKey('resetGameButton'));
    });

    test('- Remove flags on long press', () async {
      await driver.scroll(
          find.byValueKey('cell_5_5'), 0, 0, const Duration(seconds: 2));

      await driver.waitFor(find.byValueKey('cellFlagged_5_5_true'));

      await Future.delayed(const Duration(seconds: 1));

      await driver.scroll(
          find.byValueKey('cell_5_5'), 0, 0, const Duration(seconds: 2));

      await driver.waitForAbsent(find.byValueKey('cellFlagged_5_5_true'));
      await takeScreenshot('no_flag_visible_after_long_press.png');

      await driver.tap(find.byValueKey('resetGameButton'));
    });

    test('- GameOver is working', () async {
      bool gameOver = false;

      for (int i = 0; i < 10; i++) {
        if (gameOver) break;
        for (int j = 0; j < 10; j++) {
          if (!gameOver) {
            await driver.tap(find.byValueKey('cell_${i}_$j'));

            if (await isPresent(find.byValueKey('lostDialog'))) {
              gameOver = true;
              await Future.delayed(const Duration(seconds: 1));
              await takeScreenshot('game_over_dialog.png');

              await driver.tap(find.byValueKey('lostDialog'));
              break;
            }
          }
        }
      }
      expect(gameOver, true);
      await driver.tap(find.byValueKey('resetGameButton'));
    }, timeout: const Timeout(Duration(minutes: 5)));

    test('- Flag all mines and win', () async {
      bool gameWon = false;
      int flaggedMines = 0;
      int mines = 10;

      for (int i = 0; i < 10; i++) {
        if (gameWon) break;
        for (int j = 0; j < 10; j++) {
          final mine = find.byValueKey('cellIsMine_${i}_${j}_true');

          if (flaggedMines != mines) {
            if (await isPresent(mine)) {
              try {
                await driver.scroll(find.byValueKey('cell_${i}_$j'), 0, 0,
                    const Duration(seconds: 2));
              } catch (e) {
                /**/
              }
              await isPresent(find.byValueKey('cellFlagged_${i}_${j}_true'));

              flaggedMines++;

              // ignore: invariant_booleans
              if (flaggedMines == mines) {
                if (await isPresent(find.byValueKey('winDialog'))) {
                  gameWon = true;
                  await takeScreenshot('win_on_flag_mines.png');
                  await driver.tap(find.byValueKey('winDialog'));
                  break;
                }
              }
            }
          }
        }
      }
      expect(gameWon, true);
      await driver.tap(find.byValueKey('resetGameButton'));
    }, timeout: const Timeout(Duration(minutes: 5)));

    test('- Reveal all non-mine fields and win', () async {
      bool gameWon = false;
      int revealedFields = 0;

      for (int i = 0; i < 10; i++) {
        if (gameWon) break;
        for (int j = 0; j < 10; j++) {
          final nonMine = find.byValueKey('cellIsMine_${i}_${j}_false');

          if (revealedFields < 90) {
            if (await isPresent(nonMine)) {
              try {
                await driver.tap(find.byValueKey('cell_${i}_$j'));
              } catch (e) {
                /**/
              }

              revealedFields++;

              // ignore: invariant_booleans
              if (revealedFields == 90) {
                if (await isPresent(find.byValueKey('winDialog'))) {
                  gameWon = true;
                  await takeScreenshot('win_on_reveal_all.png');
                  await driver.tap(find.byValueKey('winDialog'));
                  break;
                }
              }
            }
          }
        }
      }
      expect(gameWon, true);
      await driver.tap(find.byValueKey('resetGameButton'));
    }, timeout: const Timeout(Duration(minutes: 5)));
  });
}
